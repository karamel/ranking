<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// Run these tests from cli

require_once(dirname(__FILE__) . "/../load.php");


// Say STAKE is 100

// Level 6 against level 6: 50/50, expect 50pts win and loss
// Stake is 100 at win and 100 at loss, factor level is 0.5.
echo ("Level 6-6 test: ");
$win = ELO::getWinScoreVariation(5, 5);
$los = ELO::getLoseScoreVariation(5, 5);
if ($win !== Match::STAKE / 2) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== Match::STAKE / 2) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level S against level S: 50/50, expect 10pts win and 90pts loss
// Stake is 100 at win and 100 at loss, factor 0.1 at win and 0.9 at loss
echo("Level S-S test: ");
$win = ELO::getWinScoreVariation(10, 10);
$los = ELO::getLoseScoreVariation(10, 10);
if ($win !== (int) round(0.1 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(0.9 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level 1 against 1: 50/50, expected 90pts win and 10pts loss
// Stake is 100 at win and 100 at loss, factor 0.9 at win and 0.1 at loss
echo("Level 1-1 test: ");
$win = ELO::getWinScoreVariation(0, 0);
$los = ELO::getLoseScoreVariation(0, 0);
if ($win !== (int) round(0.9 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(0.1 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level S against 1: 90/10, expected 1pt win and 810pts loss
// S is 9 time better than 1, so stakes 10 at win and 900 at loss
// win factor by 0.1 and loss by 0.9
echo ("Level S-1 test: ");
$win = ELO::getWinScoreVariation(10, 0);
$los = ELO::getLoseScoreVariation(0, 10);
if ($win !== (int) round(0.01 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(8.1 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level 1 against S: 10/90, expected 810pts win and 1pt at loss
// 1 is 0.1111... time as good as S, so stakes 900 at win and 11 at loss
// win factor by 0.9 and loss by 0.1
echo ("Level 1-S test: ");
$win = ELO::getWinScoreVariation(0, 10);
$los = ELO::getLoseScoreVariation(10, 0);
if ($win !== (int) round(8.1 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(0.01 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level 5 against 6: 42/50 (win = 84%), expected 69pts win and 35pt at loss
// 5 is 0.84 time as good as 6, so stakes 119 at win and 84 at loss
// win factor by 0.58 and loss by 0.42
echo ("Level 5-6 test: ");
$win = ELO::getWinScoreVariation(4, 5);
$los = ELO::getLoseScoreVariation(5, 4);
if ($win !== (int) round(0.69 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(0.3528 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");

// Level 6 against 5: 50/42, expected 42pts win and 60pt at loss
// 6 is 1.19 time as good as 5, so stakes 84 at win and 119 at loss
// win factor by 0.50 and loss by 0.50
echo ("Level 6-5 test: ");
$win = ELO::getWinScoreVariation(5, 4);
$los = ELO::getLoseScoreVariation(4, 5);
if ($win !== (int) round(0.42 * Match::STAKE)) {
    echo ("[FAIL] Winner variation error (" . $win . ")");
} else if ($los !== (int) round(0.595 * Match::STAKE)) {
    echo ("[FAIL] Loser variation error (" . $los . ")");
} else {
    echo ("[OK]");
}
echo ("\n");
