<?php

namespace ELOServer;

class Config {
    const DB_TYPE = "mysql";
    const DB_HOST = "localhost";
    const DB_PORT = 3306;
    const DB_NAME = "elo";
    const DB_TABLE_PREFIX = "elo_";
    const DB_USER = "user";
    const DB_PASSWORD = "password";

    /** Number of days to count games within */
    const RK_DAYS_BRACKET = 30;
    /** Show inactive players (no games in bracket) in leaderboards */
    const RK_SHOW_INACTIVE = true;

}