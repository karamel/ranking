<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(__DIR__ . "/../load.php");

// Quick and dirty api to register a player

$name = $_POST['name'];
$initials = $_POST['initials'];

$pdo = PDOBuilder::getPdo();
$stmt = $pdo->prepare("INSERT INTO elo_player (name, initials) VALUES (:name, :init)");
$stmt->bindParam(":name", $name);
$stmt->bindParam(":init", $initials);
if ($stmt->execute() === false) {
    echo "error";
    die();
}
$id = $pdo->lastInsertId();
if (isset($_FILES['avatar'])) {
    $path = __DIR__ . "/../binData/player/" . $id;
    move_uploaded_file($_FILES['avatar']['tmp_name'], $path);
}

// Return new result
$result = array();
$result['id'] = $id;
$result['name'] = $name;
$result['init'] = $initials;

echo(json_encode($result));