<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(__DIR__ . "/../load.php");

// Quick and dirty api to register a match and send back score variation

$gameId = $_POST['gameId'];
$p1Id = $_POST['p1Id'];
$p2Id = $_POST['p2Id'];
$p1Win = $_POST['p1Win'];
$p2Win = $_POST['p2Win'];
$p1WinRatio = (float)$p1Win / (float)($p1Win + $p2Win);
$p2WinRatio = (float)$p2Win / (float)($p1Win + $p2Win);

$p1Variation = 0;
$p2Variation = 0;

// Get current score
$pdo = PDOBuilder::getPdo();
$stmt = $pdo->prepare("SELECT score, level FROM elo_rank WHERE game_id = :gid "
        . "AND player_id = :pid");
$regStmt = $pdo->prepare("INSERT INTO elo_rank (game_id, player_id, score, level) VALUES "
        . "(:gid, :pid, :score, :level)");
$stmt->bindParam(":gid", $gameId);
$stmt->bindParam(":pid", $p1Id);
$regStmt->bindParam(":gid", $gameId);
$stmt->execute();
$row = $stmt->fetch();
if ($row == false) {
    $regStmt->bindParam(":pid", $p1Id);
    $regStmt->bindValue(":score", Rank::STARTING_SCORE);
    $regStmt->bindValue(":level", Rank::STARTING_LEVEL);
    $regStmt->execute();
    $p1Score = Rank::STARTING_SCORE;
    $p1Level = Rank::STARTING_LEVEL;
} else {
    $p1Score = $row['score'];
    $p1Level = $row['level'];
}
$stmt->bindParam(":pid", $p2Id);
$stmt->execute();
$row = $stmt->fetch();
if ($row == false) {
    $regStmt->bindParam(":pid", $p2Id);
    $regStmt->bindValue(":score", Rank::STARTING_SCORE);
    $regStmt->bindValue(":level", Rank::STARTING_LEVEL);
    $regStmt->execute();
    $p2Score = Rank::STARTING_SCORE;
    $p2Level = Rank::STARTING_LEVEL;
} else {
    $p2Score = $row['score'];
    $p2Level = $row['level'];
}

// Compute score variation
$p1Variation = ELO::getWinScoreVariation($p1Level, $p2Level) * $p1Win;
$p1Variation -= ELO::getLoseScoreVariation($p2Level, $p1Level) * $p2Win;
$p2Variation = ELO::getWinScoreVariation($p2Level, $p1Level) * $p2Win;
$p2Variation -= ELO::getLoseScoreVariation($p1Level, $p2Level) * $p1Win;
$p1Variation /= max($p1Win, $p2Win);
$p2Variation /= max($p1Win, $p2Win);
$p1Variation = round($p1Variation);
$p2Variation = round($p2Variation);

// Affect new score
$p1FinalScore = $p1Score + $p1Variation;
$p2FinalScore = $p2Score + $p2Variation;
if ($p1FinalScore >= $p1Score) {
    $p1FinalLevel = max(Level::getLevel($p1FinalScore), $p1Level);
} else {
    $p1FinalLevel = min(Level::getDownLevel($p1FinalScore), $p1Level);
}
if ($p2FinalScore >= $p2Score) {
    $p2FinalLevel = max(Level::getLevel($p2FinalScore), $p1Level);
} else {
    $p2FinalLevel = min(Level::getDownLevel($p2FinalScore), $p2Level);
}

// Save match and new score/rank in database
$stmt = $pdo->prepare("UPDATE elo_rank SET score = :score, level = :level "
        . "WHERE player_id = :pid AND game_id = :gid");
$stmt->bindParam(":gid", $gameId);
$stmt->bindParam(":score", $p1FinalScore);
$stmt->bindParam(":level", $p1FinalLevel);
$stmt->bindParam(":pid", $p1Id);
$stmt->execute();
$stmt->bindParam(":score", $p2FinalScore);
$stmt->bindParam(":level", $p2FinalLevel);
$stmt->bindParam(":pid", $p2Id);
$stmt->execute();
$stmt = $pdo->prepare("INSERT INTO elo_match (game_id, date, p1_id, p2_id, "
        . "p1_victories, p2_victories, p1_score, p2_score) VALUES ("
        . ":gid, now(), :p1, :p2, :p1v, :p2v, :p1s, :p2s)");
$stmt->bindParam(":gid", $gameId);
$stmt->bindParam(":p1", $p1Id);
$stmt->bindParam(":p2", $p2Id);
$stmt->bindParam(":p1v", $p1Win);
$stmt->bindParam(":p2v", $p2Win);
$stmt->bindParam(":p1s", $p1Variation);
$stmt->bindParam(":p2s", $p2Variation);
$stmt->execute();

// Return new result
$result = array();
$result['p1'] = array();
$result['p2'] = array();
$result['p1']['variation'] = $p1Variation;
$result['p1']['score'] = $p1FinalScore;
$result['p1']['oldLevel'] = $p1Level;
$result['p1']['level'] = $p1FinalLevel;
$result['p2']['variation'] = $p2Variation;
$result['p2']['score'] = $p2FinalScore;
$result['p2']['oldLevel'] = $p2Level;
$result['p2']['level'] = $p2FinalLevel;

echo(json_encode($result));