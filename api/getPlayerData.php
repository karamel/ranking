<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(__DIR__ . "/../load.php");

// Quick and dirty api to get player rank

function getPlayers($gameId = null) {
    $pdo = PDOBuilder::getPdo();
    if ($gameId !== null) {
        $stmt = $pdo->prepare("SELECT * FROM elo_player LEFT JOIN elo_rank "
                . "ON elo_player.id = elo_rank.player_id "
                . "WHERE elo_rank.game_id = :gid OR elo_rank.game_id IS NULL "
                . "ORDER BY elo_player.initials");
        $stmt->bindParam(":gid", $gameId);
        $stmt->execute();
        echo (json_encode($stmt->fetchAll(\PDO::FETCH_ASSOC)));
    }
}

if (isset($_GET['gameId'])) {
    getPlayers($_GET['gameId']);
}