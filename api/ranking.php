<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(__DIR__ . "/../load.php");

// API functions for ranking and profile data
class RankingAPI {

    /** Get ranking table for a game */
    static function getRanking($gameId, $bracket = null, $showInactive = null) {
        if ($bracket === null) {
            $bracket = Config::RK_DAYS_BRACKET;
        }
        if ($showInactive === null) {
            $showInactive = Config::RK_SHOW_INACTIVE;
        }
        $ret = NULL;
        $pdo = PDOBuilder::getPdo();
        // Get game data
        $stmtGame = $pdo->prepare("SELECT * from elo_game WHERE id = :gid");
        $stmtGame->bindParam(":gid", $gameId);
        $stmtGame->execute();
        $row = $stmtGame->fetch(\PDO::FETCH_ASSOC);
        if ($row === false) {
            $ret;
            $ret['error'] = "No game found";
        } else {
            $gameTitle = $row['name'];
        }
        $ret['game'] = array("title" => $gameTitle);
        // Get players for inactive ones
        $ret['ranking']['players'] = array();
        $stmt = $pdo->prepare("SELECT elo_player.id, name, initials, "
                . "score, level "
                . "FROM elo_player "
                . "LEFT JOIN elo_rank ON elo_player.id = elo_rank.player_id "
                . "WHERE elo_rank.game_id = :gid "
                . "ORDER BY elo_rank.score DESC, elo_player.initials DESC");
        $stmt->bindParam(":gid", $gameId);
        $stmt->execute();
        $players = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        // Get matches count
        $stmtMatches = $pdo->prepare("SELECT elo_player.id, name, initials, "
                . "score, level, count(elo_match.id) AS matches "
                . "FROM elo_player "
                . "LEFT JOIN elo_rank ON elo_player.id = elo_rank.player_id "
                . "LEFT JOIN elo_match ON (elo_match.p1_id = elo_player.id "
                . "OR elo_match.p2_id = elo_player.id) "
                . "WHERE elo_rank.game_id = :gid AND elo_match.game_id = :gid "
                . "AND elo_match.date > DATE_SUB(CURRENT_DATE(), INTERVAL " . intval($bracket) . " DAY) "
                . "GROUP BY elo_player.id "
                . "ORDER BY elo_rank.score DESC, elo_player.initials DESC");
        $stmtMatches->bindParam(":gid", $gameId);
        $stmtMatches->execute();
        $playersMatches = $stmtMatches->fetchAll(\PDO::FETCH_ASSOC);
        // Link players and match count
        if (count($playersMatches) === 0) {
            if ($showInactive) {
                for ($i = 0; $i < count($players); $i++) {
                    $players[$i]['matches'] = 0;
                    $ret['ranking']['players'][] = $players[$i];
                }
            }
            return $ret;
        }
        $j = 0;
        for ($i = 0; $i < count($players); $i++) {
            if ($players[$i]['id'] == $playersMatches[$j]['id']) {
                $players[$i]['matches'] = $playersMatches[$j]['matches'];
                $j++;
            } else {
                // No game in last days
                $players[$i]['matches'] = 0;
            }
            // Add the player to return
            if ($showInactive || $players[$i]['matches'] != 0) {
                $ret['ranking']['players'][] = $players[$i];
            }
        }
        return $ret;
    }
}
