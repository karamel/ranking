<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(__DIR__ . "/../load.php");

// Quick and dirty api to register a score

$gameId = $_POST['gameId'];
$p1Id = $_POST['p1Id'];
$p1NewLevel = $_POST['p1Level'];
$p1NewScore = $_POST['p1Score'];

// Get current score
$pdo = PDOBuilder::getPdo();
$stmt = $pdo->prepare("SELECT score, level FROM elo_rank WHERE game_id = :gid "
        . "AND player_id = :pid");
$regStmt = $pdo->prepare("INSERT INTO elo_rank (game_id, player_id, score, level) VALUES "
        . "(:gid, :pid, :score, :level)");
$stmt->bindParam(":gid", $gameId);
$stmt->bindParam(":pid", $p1Id);
$regStmt->bindParam(":gid", $gameId);
$stmt->execute();
$row = $stmt->fetch();
if ($row == false) {
    $regStmt->bindParam(":pid", $p1Id);
    $regStmt->bindValue(":score", 0);
    $regStmt->bindValue(":level", 0);
    $regStmt->execute();
    $p1Score = 0;
    $p1Level = 0;
} else {
    $p1Score = $row['score'];
    $p1Level = $row['level'];
}

// Save new score in database if better
$stmt = $pdo->prepare("UPDATE elo_rank SET score = :score, level = :level "
        . "WHERE player_id = :pid AND game_id = :gid");
$stmt->bindParam(":gid", $gameId);
$stmt->bindParam(":score", $p1NewScore);
$stmt->bindParam(":level", $p1NewLevel);
$stmt->bindParam(":pid", $p1Id);
if ($p1NewScore > $p1Score) {
    $stmt->execute();
    $result = "Record battu";
} else {
    $result = "Failed";
}
echo(json_encode($result));