<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

$gameId = $_GET['gameId'];
$game = Game::get($gameId);
$board = Rank::search(array(array("gameId", "=", $gameId)), null,
        null, array("score", "desc"));
$html = "<h1>" . $game->getName() . "</h1>";
$html .= "<table><thead><tr><th>Player</th><th>Score</th></tr></thead><tbody>";
foreach ($board as $rank) {
    $html .= "<tr><td>" . $rank->getPlayerId() . "</td><td>" . $rank->getScore() . "</td></tr>";
}
$html .= "</tbody></table>";
Page::$output = $html;
