<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

class Match extends AbstractService {

    protected static $dbTable = "match";
    protected static $dbIdField = "id";
    protected static $fieldMapping = array(
            "id" => "id",
            "date" => array("type" => "date", "attr" => "date"),
            "game_id" => "gameId",
    );

    protected function build($row, $pdo = null) {
        $db = DB::get();
        return new Match($row["id"], $db->readDate($row["date"]),
                $row['gameId']);
    }

    /** Normal amount of point in play for a match */
    const STAKE = 200;

    private $id;
    private $date;
    private $gameId;

    public function __construct($id, $date, $gameId) {
        $this->id = $id;
        $this->date = $date;
        $this->gameId = $gameId;
    }

    /** Create add a match and return [winner rank, loser rank] */
    public function create($gameId, $date, $winnerId, $winnerLevel,
            $winnerScore, $loserId, $loserLevel, $loserScore) {
        $newTransaction = !$pdo->inTransaction();
        if ($newTransaction) {
            $pdo->beginTransaction();
        }
        // Create match
        $id = super.create(new Game(null, $date, $gameId));
        if ($id === false) {
            if ($newTransaction) {
                $pdo->rollback();
            }
            return false;
        }
        // Get result
        $result = ELO::getScoreVariation($winnerLevel, $loserLevel);
        // Add encounters
        $wEnc = new Encounter($id, $winnerId, $winnerLevel,
                $winnerScore, 1, $result);
        $lEnc = new Encounter($id, $loserId, $loserLevel,
                $loserScore, 2, -$result);
        if ($wEnc === false || $lEnc === false) {
            if ($newTransaction) {
                $pdo->rollback();
            }
            return false;
        }
        // Update player scores
        $wNewScore = $winnerScore + $result;
        $wNewLevel = Level::getLevel($wNewScore);
        $wRank = new Rank($winnerId, $gameId, $wNewLevel, $wNewScore);
        $wUpd = Rank::update($wRank);
        $lNewScore = $loserScore - $result;
        $lNewLevel = Level::getDownLevel($lNewLevel);
        $lRank = new Rank($loserId, $gameId, $lNewLevel, $lNewScore);
        $lUpd = Rank::update($lRank);
        if ($wUpd === false || $lUpd === false) {
            if ($newTransaction) {
                $pdo->rollback();
            }
            return false;
        }
        if ($newTransaction) {
            $pdo->commit();
        }
        return array($wRank, $lRank);
    }
}

class Encounter extends AbstractService {

    protected static $dbTable = "encounter";
    protected static $dbIdField = "id";
    protected static $fieldMapping = array(
            "match_id" => "matchId",
            "player_id" => "playerId",
            "player_level" => "playerLevel",
            "player_score" => "playerScore",
            "rank" => "rank",
            "result" => "result",
    );

    protected function build($row, $pdo = null) {
        $db = DB::get();
        return new Encounter($row["match_id"], $row["player_id"],
                $row['player_level'], $row['player_score'],
                $row['rank'], $row['result']);
    }
    private $matchId;
    private $playerId;
    private $playerLevel;
    /** The score of the player just before the match. */
    private $playerScore;
    /** The rank of the player in the match */
    private $rank;
    /** The ELO variation for this player. */
    private $result;

    public function __construct($matchId, $playerId, $playerLevel,
            $playerScore, $rank, $result) {
        $this->matchId = $matchId;
        $this->playerId = $playerId;
        $this->playerLevel = $playerLevel;
        $this->playerScore = $playerScore;
        $this->rank = $rank;
        $this->result = $result;
    }
}