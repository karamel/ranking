<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

class Player extends AbstractService {

    protected static $dbTable = "player";
    protected static $dbIdField = "id";
    protected static $fieldMapping = array(
            "id" => "id",
            "name" => "name",
    );

    protected function build($row, $pdo = null) {
        return new Player($row["id"], $row["name"]);
    }

    private $id;
    private $name;

    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
}