<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

class Rank extends AbstractService {

    protected static $dbTable = "rank";
    protected static $dbIdField = "playerId";
    protected static $fieldMapping = array(
            "player_id" => "playerId",
            "game_id" => "gameId",
            "level" => "level",
            "score" => "score",
    );

    protected function build($row, $pdo = null) {
        return new Rank($row["player_id"], $row["game_id"],
                $row['level'], $row['score']);
    }

    const STARTING_LEVEL = 5;
    const STARTING_SCORE = 2500;
    const MAX_SCORE = 5000;

    private $playerId;
    private $gameId;
    private $level;
    private $score;

    public function __construct($playerId, $gameId, $level, $score) {
        $this->playerId = $playerId;
        $this->gameId = $gameId;
        $this->level = $level;
        $this->score = $score;
    }

    public function getPlayerId() {
        return $this->playerId;
    }
    public function getGameId() {
        return $this->gameId;
    }
    /** Get curent level, it may differ from the one computed from score. */
    public function getLevel() {
        return $this->level;
    }
    public function getScore() {
        return $this->score;
    }
}

class Level {

    /** Step from one level to the next */
    const STEP = 500;

    /** Get the level associated to a given score */
    public static function getLevel($score) {
        return min(10, floor($score / Level::STEP));
    }
    /** Get the level associated to a given score when losing points */
    public static function getDownLevel($score) {
        $level = Level::getLevel($score);
        if ($score > ($level * Level::STEP + (2 * floor(Level::STEP / 3)))) {
            // Let the player loose some points before leveling down
            return $level + 1;
        }
        return $level;
    }

    public static function getLevelName($level) {
        if ($level < 10) {
            return $level + 1;
        } else {
            return "S";
        }
    }

    /** Get win ratio for the given level,
     * lose ratio is 1.0 - win ratio. */
    public static function getWinRatio($level) {
        // Level 0: 10% win
        // Level 6: 50% win
        // Level 10: 10% win
        return 0.1 + 0.08 * $level;
    }
}