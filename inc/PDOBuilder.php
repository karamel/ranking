<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.
//    Pastèque Web back office

namespace ELOServer;

class PDOBuilder {

    private static $pdo = NULL;

    /** Get PDO from the loaded database core module */
    public static function getPDO() {
        if (PDOBuilder::$pdo !== NULL) {
            return PDOBuilder::$pdo;
        }
        $dsn = null;
        switch (Config::DB_TYPE) {
        case 'mysql':
            $dsn = "mysql:dbname=" . Config::DB_NAME . ";host="
                    . Config::DB_HOST . ";port=" . Config::DB_PORT;
            $options = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'');
            break;
        case 'postgresql':
            $dsn = "pgsql:dbname=" . Config::DB_NAME . ";host="
                    . Config::DB_HOST . ";port=" . Config::DB_PORT;
            $options = array();
            break;
        default:
            die("Config error");
        }
        try {
            PDOBuilder::$pdo = new \PDO($dsn, Config::DB_USER,
                    Config::DB_PASSWORD, $options);
            return PDOBuilder::$pdo;
        } catch (\PDOException $e) {
            die("Connexion error " . $e);
        }
    }

}
