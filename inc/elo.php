<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

class ELO {

    /** Get win probability for a player according to levels.
     * 1 meaning challenger is 1 time as performant as opponent. */
    private static function getWinProbabilityFactor($challenger, $opponent) {
        $challengerRatio = Level::getWinRatio($challenger);
        $opponentRatio = Level::getWinRatio($opponent);
        return $challengerRatio / $opponentRatio;
    }

    /** Get the absolute score variation for a game.
     * Winner will earn the variation while loser will lose it.
     */
    public static function getWinScoreVariation($winnerLevel, $loserLevel) {
        // Affect respective probability to score
        $levelStake = Match::STAKE / ELO::getWinProbabilityFactor($winnerLevel, $loserLevel);
        // Adjust win/lose according to level
        /* Winner ratio is used to keep win/lose variation
         * at the same level (i.e 90% win ratio = 9 win/1 lose
         * will result in a 0 variation at the same level). */
        $winRatio = Level::getWinRatio($winnerLevel);
        return (int) round($levelStake * (1.0 - $winRatio));
    }

    public static function getLoseScoreVariation($winnerLevel, $loserLevel) {
        // Affect respective probability to score
        $levelStake = Match::STAKE * ELO::getWinProbabilityFactor($loserLevel, $winnerLevel);
        // Adjust win/lose according to level
        /* Winner ratio is used to keep win/lose variation
         * at the same level (i.e 90% win ratio = 9 win/1 lose
         * will result in a 0 variation at the same level). */
        $winRatio = Level::getWinRatio($loserLevel);
        return (int) round($levelStake * $winRatio);
    }
}