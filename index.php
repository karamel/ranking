<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(dirname(__FILE__) . "/load.php");

$page = "home";
if (isset($_GET['p'])) {
    $page = $_GET['p'];
}

$acceptedPages = array('home', 'leaderboard');

if (!in_array($page, $acceptedPages)) {
    header("403 Forbidden");
    die("Forbidden");
}

require_once(dirname(__FILE__) . "/pages/" . $page . ".php");


?>
<html>
    <head>
<?php foreach (Page::$scripts as $script) { ?>
        <script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
    </head>
    <body>
        <div id="content">
<?php echo Page::$output; ?>
        </div>
    </body>
</html>