<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

$ABSPATH = __DIR__;

// Load all core files
require_once(dirname(__FILE__) . "/config.php");
require_once(dirname(__FILE__) . "/inc/PDOBuilder.php");
require_once(dirname(__FILE__) . "/inc/DB.php");
require_once(dirname(__FILE__) . "/inc/AbstractService.php");
require_once(dirname(__FILE__) . "/inc/models/game.php");
require_once(dirname(__FILE__) . "/inc/models/player.php");
require_once(dirname(__FILE__) . "/inc/models/rank.php");
require_once(dirname(__FILE__) . "/inc/models/match.php");
require_once(dirname(__FILE__) . "/inc/elo.php");
require_once(dirname(__FILE__) . "/pages/page.php");

// Load API
// TODO: load API only when needed
require_once(dirname(__FILE__) . "/api/ranking.php");