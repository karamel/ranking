<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// This is a quick and dirty score table

require_once(dirname(__FILE__) . "/../load.php");

$pdo = PDOBuilder::getPdo();
$gameId = $_GET['gameId'];
if (isset($_GET['rotation'])) {
    $rotation = explode(',', $_GET['rotation']);
} else {
    $rotation = null;
}

$data = RankingAPI::getRanking($gameId, null, true);
$game = $data['game'];
$players = $data['ranking']['players'];
?>
<html>
<head>
<meta charset="utf-8" />
<title>ELO Server</title>
<script type="text/javascript" src="../libs/jquery-1.11.2.min.js"></script>
<link rel="stylesheet" href="../res/style.css" />
<style type="text/css">
body {
	overflow: hidden;
}
#background {
	background-image: url("../res/images.php?game=<?php echo $gameId; ?>");
}
#leaderboard {
	background-color: #333;
	margin: auto;
	width: 80ex;
}
#leaderboard th,
#leaderboard td {
	padding: 0.5ex;
}
#leaderboard th {
	text-align: center;
	font-variant: small-caps;
	font-weight: bold;
}
#leaderboard .rank {
	width: 2ex;
	font-size: 150%;
}
#leaderboard .avatar {
	width: 64px;
}
#leaderboard .initials {
	width: 5ex;
	font-weight: bold;
}
#leaderboard .level {
	width : 3ex;
	font-weight: bold;
}
#leaderboard .score {
	width: 12ex;
	font-weight: bold;
}
#leaderboard .rank,
#leaderboard .initials,
#leaderboard .level {
	text-align: center;
}
#leaderboard .score {
	text-align: right;
	padding-right: 1ex;
}
</style>
</head>
<body style="text-align:center;margin-top:5ex;margin-bottom:5ex;">
<div id="background"></div>
<table id="leaderboard" border="1" cellspacing="0">
	<thead>
		<tr>
			<th>#</th>
			<th colspan="3">Joueur</th>
			<th>Stage</th>
			<th>Score</th>
		</tr>
	</thead>
	<tbody>
<?php $i = 0; foreach ($players as $player) { $i++; ?>
		<tr>
			<td class="rank"><?php echo $i; ?></td>
			<td class="avatar"><img src="../res/images.php?player=<?php echo $player['id']; ?>" width="64" height="64" /></td>
			<td class="initials"><?php echo htmlspecialchars($player['initials']); ?></td>
			<td class="name"><?php echo htmlspecialchars($player['name']); ?></td>
			<td class="level"><?php echo $player['level']; ?></td>
			<td class="score"><?php echo $player['score']; ?></td>
		</tr>
<?php } ?>
	</tbody>
</table>
<script type="text/javascript" src="move_background.js"></script>
<script type="text/javascript">
var offset = 0;
var bottomOffset = jQuery("body")[0].scrollHeight;
var scrollUpInterval;
var cycles = 0;
var refreshAt = 3;
function scrollBottom() {
	jQuery("body").animate({
		scrollTop: jQuery("tr:last").offset().top
	}, 1000, "swing", function() { scrollTop(); });
}
function scrollUp() {
	var body = jQuery("body");
	if (body.scrollTop() == 0) {
		clearInterval(scrollUpInterval);
		cycles++;
		if (cycles == refreshAt) {
<?php if ($rotation !== null) {
    $newLocation = "http://" . $_SERVER['SERVER_NAME'] . ":"
        . $_SERVER['SERVER_PORT'] . $_SERVER['PHP_SELF'] . '?';
    $nextGameId = $rotation[0];
    for ($i = 0; $i < count($rotation); $i++) {
        if ($rotation[$i] == $gameId) {
            $nextGameId = $rotation[(($i + 1) % count($rotation))];
        }
    }
    $newLocation .= "rotation=" . implode(',', $rotation);
    $newLocation .= "&gameId=" . $nextGameId;
 ?>
			window.location = "<?php echo $newLocation; ?>";
<?php } else { ?>			
			window.location.reload(true);
<?php } ?>
		}
		setTimeout("scrollBottom()", 5000);
		return;
	}
	body.scrollTop(body.scrollTop() - 1);
}
function scrollTop() {
	scrollUpInterval = setInterval("scrollUp()", 30);
}
scrollBottom();
</script>
</body>
</html>
