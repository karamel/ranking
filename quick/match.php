<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// This is a quick and dirty form to register results

require_once(dirname(__FILE__) . "/../load.php");

$pdo = PDOBuilder::getPdo();
$gameId = $_GET['gameId'];
$stmtPlayers = $pdo->prepare("SELECT * FROM elo_player "
        . "ORDER BY elo_player.initials");
$stmtScores = $pdo->prepare("SELECT * FROM elo_player LEFT JOIN elo_rank "
        . "ON elo_player.id = elo_rank.player_id "
        . "WHERE elo_rank.game_id = :gid "
        . "ORDER BY elo_player.initials");
$stmtPlayers->execute();
$stmtScores->bindParam(":gid", $gameId);
$stmtScores->execute();

$players = $stmtPlayers->fetchAll(\PDO::FETCH_ASSOC);
$scores = $stmtScores->fetchAll(\PDO::FETCH_ASSOC);

$j = 0;
for ($i = 0; $i < count($players); $i++) {
    if (count($scores) == 0 || $j >= count($scores)
            || $scores[$j]['player_id'] != $players[$i]['id']) {
        // This player has no score yet
        $players[$i]['level'] = Level::getLevelName(RANK::STARTING_LEVEL);
        $players[$i]['score'] = Rank::STARTING_SCORE;
    } else {
        $players[$i]['level'] = Level::getLevelName($scores[$j]['level']);
        $players[$i]['score'] = $scores[$j]['score'];
        $j++;
    }
}
?>
<html>
<head>
<meta charset="utf-8" />
<title>ELO Server</title>
<script type="text/javascript" src="../libs/jquery-1.11.2.min.js"></script>
<link rel="stylesheet" href="../res/style.css" />
<style type="text/css">
#background {
	background-image: url("../res/images.php?game=<?php echo $gameId; ?>");
}
#p1block,
#p2block {
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	width: 300px;
	padding-bottom: 1ex;
	padding-top: 22px;
	background-color: #333;
	border: 2px outset #ddd;
}
#p1block select, #p2block select {
	display: block;
	width: 80%;
	margin: 1ex auto;
}

ul.playerdata {
	margin: 0px auto;
	padding: 0px;
	width: 256px;
	list-style-type: none;
}
ul.playerdata li {
	display: inline-block;
	height: 64px;
	line-height: 64px;
	margin: 0px;
	text-align: center;
}
ul.playerdata .initials {
	width: 112px;
	background-color: #222;
	font-weight: bold;
}
ul.playerdata .level {
	width: 144px;
	background-color: #444;
}
#versusblock {
	display:inline-block;
	width: 200px;
	text-align:center;
}
#sendbtn {
	display: block;
	width: 200px;
	height: 64px;
	margin: 5ex auto 1ex auto;
}
#resultblock {
	background-color: #333;
	margin: auto;
	border: 1px solid #fff;
}
</style>
<script type="text/javascript">
var players = new Array();
<?php foreach ($players as $player) {
    echo ("players['" . $player['id'] . "'] = " . json_encode($player) . ";\n");
} ?>
</script>
</head>
<body style="text-align:center;margin-top:5ex;">
<div id="background"></div>
<form action="#" method="post" onSubmit="javascript:send();return false;">
	<div id="p1block">
		<img id="p1img" width="256" height="256" src="../res/images/unknown.png" />
		<ul class="playerdata" id="p1data">
			<li id="p1init" class="initials">???</li><li class="level">Niveau <span id="p1lvl">inconnu</span></li>
		</ul>
		<select id="p1" name="p1" onChange="javascript:playerSwitch(1);" tabindex="1">
			<option value="">Joueur 1</option>
			<?php foreach ($players as $player) { ?>
			<option value="<?php echo htmlspecialchars($player['id']) ?>"><?php echo htmlspecialchars($player['initials']); ?> - <?php echo htmlspecialchars($player['name']); ?></option>
			<?php } ?>
		</select>
		<label for="p1win">Victoires</label>
		<input type="number" name="p1win" id="p1win" tabindex="3" />
	</div>
	<div id="versusblock">
		<img src="../res/images/versus.png" />
	</div>
	<div id="p2block">
		<img id="p2img" width="256" height="256" src="../res/images/unknown.png" />
		<ul class="playerdata" id="p2data">
			<li id="p2init" class="initials">???</li><li class="level">Niveau <span id="p2lvl">inconnu</span></li>
		</ul>
		<select id="p2" name="p2" onChange="javascript:playerSwitch(2);" tabindex="2">
			<option value="">Joueur 2</option>
			<?php foreach ($players as $player) { ?>
			<option value="<?php echo htmlspecialchars($player['id']) ?>"><?php echo htmlspecialchars($player['initials']); ?> - <?php echo htmlspecialchars($player['name']); ?></option>
			<?php } ?>
		</select>
		<label for="p2win">Victoires</label>
		<input type="number" name="p2win" id="p2win" tabindex="4" />
	</div>
	<div id="sendblock">
		<input id="sendbtn" type="submit" value="GO !" />
	</div>
	<div id="resultblock" style="display:none;">
		<p id="p1result"></p>
		<p id="p2result"></p>
	</div>
</form>
<script type="text/javascript">
function reset() {
	jQuery("#p1").val("");
	jQuery("#p2").val("");
	playerSwitch(1);
	playerSwitch(2);
}
function playerSwitch(playerNum) {
	var playerId = jQuery("#p" + playerNum).val();
	if (playerId == "") {
		jQuery("#p" + playerNum + "img").attr("src", "../res/images/unknown.png");
		jQuery("#p" + playerNum + "init").html("???");
		jQuery("#p" + playerNum + "lvl").html("inconnu");
		return;
	}
	var src = "res/images.php?player=" + playerId;
	jQuery("#p" + playerNum + "img").attr("src", "../" + src);
	jQuery("#p" + playerNum + "init").html(players[playerId]['initials']);
	var level = players[playerId]["level"];
	if (level == null) {
		level = "inconnu";
	}
	jQuery("#p" + playerNum + "lvl").html(level);
}
function send() {
	var p1Id = jQuery("#p1").val();
	var p2Id = jQuery("#p2").val();
	var p1Win = parseInt(jQuery("#p1win").val(), 10);
	var p2Win = parseInt(jQuery("#p2win").val(), 10);
	if (p1Id == "" || p2Id == "" || (p1Win == 0 && p2Win == 0)) {
		return;
	}
	jQuery.ajax("../api/registerMatch.php", {
		"type": "POST",
		"data": {"p1Id": p1Id, "p2Id": p2Id, "p1Win": p1Win, "p2Win": p2Win, "gameId": <?php echo $gameId; ?>},
		"error": function(jqXHR, textStatus, errorThrown) { alert(textStatus); },
		"success": function(data) {
			var result = JSON.parse(data);
			showResult(result.p1, result.p2);
		}
	});
}
function showResult(p1, p2) {
	var p1LvlUp = (p1.level > p1.oldLevel);
	var p2LvlUp = (p2.level > p2.oldLevel);
	var p1LvlDown = (p1.level < p1.oldLevel);
	var p2LvlDown = (p2.level < p2.oldLevel);
	var p1String = jQuery("#p1 option:selected").text();
	var p2String = jQuery("#p2 option:selected").text();
	if (p1.variation >= 0) {
		p1String += " <span style=\"color:#2a2\">+" + p1.variation + " points</span>";
	} else {
		p1String += " <span style=\"color:#a22\">" + p1.variation + " points</span>";
	}
	if (p1LvlUp) {
		p1String += " <span style=\"font-weight:bold;color:#2a2\">LEVEL UP</span>";
	}
	if (p1LvlDown) {
		p1String += " <span style=\"font-weight:bold;color:#a22\">LEVEL DOWN</span>";
	}
	if (p2.variation >= 0) {
		p2String += " <span style=\"color:#2a2\">+" + p2.variation + " points</span>";
	} else {
		p2String += " <span style=\"color:#a22\">" + p2.variation + " points</span>";
	}
	if (p2LvlUp) {
		p2String += " <span style=\"font-weight:bold;color:#2a2\">LEVEL UP</span>";
	}
	if (p2LvlDown) {
		p2String += " <span style=\"font-weight:bold;color:#a22\">LEVEL DOWN</span>";
	}
	jQuery("#p1result").html(p1String);
	jQuery("#p2result").html(p2String);
	jQuery("#resultblock").show();
	reset();
}

reset();
</script>
<script type="text/javascript" src="move_background.js"></script>
</body>
</html>