<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

require_once(dirname(__FILE__) . "/../load.php");

$pdo = PDOBuilder::getPdo();

$stmt = $pdo->prepare("SELECT * from elo_game ORDER BY name ASC");
$stmt->execute();
$games = array();
while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
    $games[] = $row;
}

?>
<html>
<head>
<meta charset="utf-8" />
<title>ELO Server</title>
<link rel="stylesheet" href="../res/style.css" />
<body style="text-align:center;margin-top:5ex;margin-bottom:5ex;">
<div id="background"></div>
<h1 id="gametitle">Admin</h1>
<h2>Games and players</h2>
<ul>
	<li><a href="addPlayer.php" target="_blank">Add player</a></li>
</ul>
<h2>Register game</h2>
<ul>
<?php foreach ($games as $game) { ?>
	<li><a href="match.php?gameId=<?php echo $game['id']; ?>"><?php echo htmlspecialchars($game['name']); ?></a></li>
<?php } ?> 
</ul>