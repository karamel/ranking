<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// This is a quick and dirty form to register a new player

require_once(dirname(__FILE__) . "/../load.php");

?>
<html>
<head>
<meta charset="utf-8" />
<title>ELO Server</title>
<script type="text/javascript" src="../libs/jquery-1.11.2.min.js"></script>
<link rel="stylesheet" href="../res/style.css" />
</head>
<body style="text-align:center;margin-top:5ex;">
<p id="message"></p>
<form action="../api/registerPlayer.php" method="post" enctype="multipart/form-data">
	<ul>
		<li>
			<label for="name">Nom/Pseudo</label>
			<input type="text" id="name" name="name" />
		</li>
		<li>
			<label for="initials">Initiales</label>
			<input type="text" id="initials" name="initials" />
		</li>
		<li>
			<label for="avatar">Avatar</label>
			<input type="file" id="avatar" name="avatar" />
		</li>
		<li>
			<input type="submit" value="Enregistrer" />
		</li>
	</ul>
</form>
</body>
</html>