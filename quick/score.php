<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// This is a quick and dirty form to register results

require_once(dirname(__FILE__) . "/../load.php");

$pdo = PDOBuilder::getPdo();
$gameId = $_GET['gameId'];
$stmtPlayers = $pdo->prepare("SELECT * FROM elo_player "
        . "ORDER BY elo_player.initials");
$stmtScores = $pdo->prepare("SELECT * FROM elo_player LEFT JOIN elo_rank "
        . "ON elo_player.id = elo_rank.player_id "
        . "WHERE elo_rank.game_id = :gid "
        . "ORDER BY elo_player.initials");
$stmtPlayers->execute();
$stmtScores->bindParam(":gid", $gameId);
$stmtScores->execute();

$players = $stmtPlayers->fetchAll(\PDO::FETCH_ASSOC);
$scores = $stmtScores->fetchAll(\PDO::FETCH_ASSOC);

$j = 0;
for ($i = 0; $i < count($players); $i++) {
    if (count($scores) == 0 || $j >= count($scores)
            || $scores[$j]['player_id'] != $players[$i]['id']) {
        // This player has no score yet
        $players[$i]['level'] = 0;
        $players[$i]['score'] = 0;
    } else {
        $players[$i]['level'] = $scores[$j]['level'];
        $players[$i]['score'] = $scores[$j]['score'];
        $j++;
    }
}
?>
<html>
<head>
<meta charset="utf-8" />
<title>ELO Server</title>
<script type="text/javascript" src="../libs/jquery-1.11.2.min.js"></script>
<link rel="stylesheet" href="../res/style.css" />
<style type="text/css">
#background {
	background-image: url("../res/images.php?game=<?php echo $gameId; ?>");
}
#p1block,
#p2block {
	display: inline-block;
	text-align: center;
	vertical-align: middle;
	width: 300px;
	padding-bottom: 1ex;
	padding-top: 22px;
	background-color: #333;
	border: 2px outset #ddd;
}
#p1block select, #p2block select {
	display: block;
	width: 80%;
	margin: 1ex auto;
}

ul.playerdata {
	margin: 0px auto;
	padding: 0px;
	width: 256px;
	list-style-type: none;
}
ul.playerdata li {
	display: inline-block;
	height: 64px;
	line-height: 64px;
	margin: 0px;
	text-align: center;
}
ul.playerdata .initials {
	width: 256px;
	background-color: #222;
	font-weight: bold;
}
ul.playerdata .level {
	width: 112px;
	background-color: #222;
	font-weight: bold;
}
ul.playerdata .score {
	width: 144px;
	background-color: #444;
}
#versusblock {
	display:inline-block;
	width: 200px;
	text-align:center;
}
#sendbtn {
	display: block;
	width: 200px;
	height: 64px;
	margin: 5ex auto 1ex auto;
}
#resultblock {
	background-color: #333;
	margin: auto;
	border: 1px solid #fff;
}
</style>
<script type="text/javascript">
var players = new Array();
<?php foreach ($players as $player) {
    echo ("players['" . $player['id'] . "'] = " . json_encode($player) . ";\n");
} ?>
</script>
</head>
<body style="text-align:center;margin-top:5ex;">
<div id="background"></div>
<form action="#" method="post" onSubmit="javascript:send();return false;">
	<div id="p1block">
		<img id="p1img" width="256" height="256" src="../res/images/unknown.png" />
		<ul class="playerdata" id="p1data">
			<li id="p1init" class="initials">???</li><li class="level">Niv. <span id="p1lvl">?</span></li><li class="score"><span id="p1scr">?</span></li>
		</ul>
		<select id="p1" name="p1" onChange="javascript:playerSwitch(1);" tabindex="1">
			<option value="">Joueur 1</option>
			<?php foreach ($players as $player) { ?>
			<option value="<?php echo htmlspecialchars($player['id']) ?>"><?php echo htmlspecialchars($player['initials']); ?> - <?php echo htmlspecialchars($player['name']); ?></option>
			<?php } ?>
		</select>
		<label for="p1level">Niveau</label>
		<input type="number" name="p1level" id="p1level" tabindex="3" />
		<br />
		<label for="p1score">Score</label>
		<input type="number" name="p1score" id="p1score" tabindex="4" />
	</div>
	<div id="sendblock">
		<input id="sendbtn" type="submit" value="GO !" />
	</div>
	<div id="resultblock" style="display:none;">
		<p id="result"></p>
	</div>
</form>
<script type="text/javascript">
function reset() {
	jQuery("#p1").val("");
	playerSwitch(1);
}
function playerSwitch(playerNum) {
	var playerId = jQuery("#p" + playerNum).val();
	if (playerId == "") {
		jQuery("#p" + playerNum + "img").attr("src", "../res/images/unknown.png");
		jQuery("#p" + playerNum + "init").html("???");
		jQuery("#p" + playerNum + "lvl").html("?");
		jQuery("#p" + playerNum + "scr").html("?");
		return;
	}
	var src = "res/images.php?player=" + playerId;
	jQuery("#p" + playerNum + "img").attr("src", "../" + src);
	jQuery("#p" + playerNum + "init").html(players[playerId]['initials']);
	var level = players[playerId]["level"];
	var score = players[playerId]["score"];
	if (level == null) {
		level = "?";
	}
	if (score == null) {
		score = "";
	}
	jQuery("#p" + playerNum + "lvl").html(level);
	jQuery("#p" + playerNum + "scr").html(score);
}
function send() {
	var p1Id = jQuery("#p1").val();
	var p1Score = parseInt(jQuery("#p1score").val(), 10);
	var p1Level = parseInt(jQuery("#p1level").val(), 10);
	if (p1Id == "" || p1Level == 0 || p1score == 0) {
		return;
	}
	jQuery.ajax("../api/registerScore.php", {
		"type": "POST",
		"data": {"p1Id": p1Id, "p1Score": p1Score, "p1Level": p1Level, "gameId": <?php echo $gameId; ?>},
		"error": function(jqXHR, textStatus, errorThrown) { alert(textStatus); },
		"success": function(data) {
			//var result = JSON.parse(data);
			showResult(data);
		}
	});
}

function showResult(result) {
	jQuery("#result").html(result);
	jQuery("#resultblock").show();
	reset();
}

reset();
</script>
<script type="text/javascript" src="move_background.js"></script>
</body>
</html>