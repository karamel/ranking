<?php
//    Copyright (C) 2014, see AUTHOR for contributors
//
//    This file is part of ELO Server.
//
//    ELO Server is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ELO Server is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ELOServer;

// API to get images resources from an url

function getGameLogo($gameId) {
    $sanitizedId = (int) $gameId;
    if ($sanitizedId === 0) {
        return;
    }
    $path = __DIR__ . "/../binData/game/" . $sanitizedId;
    if (file_exists($path)) {
        echo file_get_contents($path);
    } else {
        
    }
}

function getPlayerPortrait($playerId) {
    $sanitizedId = (int) $playerId;
    if ($sanitizedId === 0) {
        return;
    }
    $path = __DIR__ . "/../binData/player/" . $sanitizedId;
    if (file_exists($path)) {
        echo file_get_contents($path);
    } else {
        echo file_get_contents(__DIR__ . "/images/unknown.png");
    }
}


if (isset($_GET['player'])) {
    getPlayerPortrait($_GET['player']);
} else if (isset($_GET['game'])) {
    getGameLogo($_GET['game']);
}