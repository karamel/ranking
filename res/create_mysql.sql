CREATE TABLE IF NOT EXISTS `elo_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `elo_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `initials` varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `elo_rank` (
  `player_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (player_id, game_id),
  CONSTRAINT rank_fk_player FOREIGN KEY (player_id) REFERENCES elo_player(id),
  CONSTRAINT rank_fk_game FOREIGN KEY (game_id) REFERENCES elo_game(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `elo_match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `game_id` int(11) NOT NULL,
  `p1_id` int(11) NOT NULL,
  `p2_id` int(11) NOT NULL,
  `p1_victories` int(11) NOT NULL,
  `p2_victories` int(11) NOT NULL,
  `p1_score` int(11) NOT NULL,
  `p2_score` int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY match_fk_game (game_id) REFERENCES elo_game(id),
  FOREIGN KEY match_fk_winner (p1_id) REFERENCES elo_player(id),
  FOREIGN KEY match_fk_loser (p2_id) REFERENCES elo_player(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
